var Calculator = function(sum) {
    this.sum = +sum || 0;
};

Calculator.prototype.add = function(num) {
    this.sum += num;
    return this;
};

Calculator.prototype.substract = function(num) {
    this.sum -= num;
    return this;
};

Calculator.prototype.multiply = function(num) {
    this.sum *= num;
    return this;
};

Calculator.prototype.divide = function(num) {
    this.sum /= num;
    return this;
};




var calc = new Calculator('0');
var result = calc.add(10).add(1).substract(5).multiply(10);


console.log(result.sum);