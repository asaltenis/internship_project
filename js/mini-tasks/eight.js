function numberX(x) {

    return function numberY(y) {
        return x + y;
    };
}

console.log(numberX(11)(10));