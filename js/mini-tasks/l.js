// 1 - I, 2 - II, 3 - III, 4 - IV, 5 - V, 6 - VI, ..., 9 - IX

// 10 - X, xx xxx xl l lx lxx lxxx xc
// 50 - L,
// 100 - C

var romanNum = ['I', 1, 'IV', 4, 'V', 5];

function roman(num) {
    for (var i = 1; i < romanNum.length; i += 2) {
        if (romanNum[i] === num) {
            return romanNum[i - 1];
        } else if (romanNum[i] + romanNum[i] === num) {
            return romanNum[i - 1] + romanNum[i - 1];
        } else if (romanNum[i] + romanNum[i] + romanNum[i] === num) {
            return romanNum[i - 1] + romanNum[i - 1] + romanNum[i - 1];
        }
    }
}

roman(3);