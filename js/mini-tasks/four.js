function suma(arr) {
    var sum = 0;

    for (var i = 0; i < arr.length; i++) {
        sum += arr[i];
    }
    console.log(sum);
}

suma([1, 2, 3]);


// reduce prototype

function sumb(arr) {
    console.log(arr.reduce(sum));
}

function sum(result, num) {
    return result + num;
}

sumb([1, 2, 3, 6, 7, 8]);


// reduce es6

const arr = [1,2,3,4,5];
const reducer = (a, curVal) => a + curVal;

console.log(arr.reduce(reducer));