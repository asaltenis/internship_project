function arrSum(arr) {
    var sum = 0;

    arr.forEach(function(element) {
        if (element.constructor === Array) {
            sum += arrSum(element);
        } else {
            sum += element;
        } 
    });

    return sum;
}

console.log(arrSum([10, 6, [4, 8], 3, [6, 5, [9,8,[10,[5,[10]]]]]]));