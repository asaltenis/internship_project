function say(text) {

    return function add(otherText) {
        return text + ' ' + otherText;
    };
}

console.log(say('Hello,')('it\'s me'));