function stuff(something) {
    console.log('Doing ' + something);
}

var spy = function(spiedFunc) {
    var totalCalls = 0;
    var result = function() {
        totalCalls++;
        return spiedFunc.apply(this, arguments);
    };
    result.report = function () {
        return {
            totalCalls : totalCalls
        };
    };
    return result;
};

var spied = spy(stuff);

spied('something');
spied('something');
spied('something');
spied('something');
spied('something');
spied('something');
spied('something');

console.log(spied.report());