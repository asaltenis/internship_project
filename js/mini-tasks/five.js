var houses = [
    { name: 'Targaryen', motto: 'Fire and Blood' },
    { name: 'Stark', motto: 'Winter is Coming' },
    { name: 'Bolton', motto: 'Our Blades Are Sharp' },
    { name: 'Greyjoy', motto: 'We Do Not Sow' },
    { name: 'Tully', motto: 'Family, Duty, Honor' },
    { name: 'Arryn', motto: 'As High as Honor' },
    { name: 'Lannister', motto: 'Hear Me Roar!' },
    { name: 'Tyrell', motto: 'Growing Strong' },
    { name: 'Baratheon', motto: 'Ours is the Fury' },
    { name: 'Martell', motto: 'Unbowed, Unbent, Unbroken' }
];

// es5

function motto(house) {
    for (var i = 0; i < houses.length; i++) {
        if (house === houses[i].name) {
            console.log(houses[i].motto);
        }
    }
}

// es6

function mottoEs6(house) {
    console.log(houses.find(e => e.name === house).motto);
}

motto('Tyrell');
mottoEs6('Tully');