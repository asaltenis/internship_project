export default function () {
    var itemTitle = document.querySelector('#title');
    var itemAuthor = document.querySelector('#author');
    var itemContent = document.querySelector('#content');

    document.querySelector('#add-button').addEventListener('click', function () {
        fetch('http://localhost:3000/posts', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                title: itemTitle.value,
                author: itemAuthor.value,
                content: itemContent.value
            })
        }).then(function (response) {
            response.json()
                .then(function () {
                    if (!itemTitle.value || !itemAuthor || !itemContent) {
                        itemTitle.value ? itemTitle.style.backgroundColor = '#fff' : itemTitle.style.backgroundColor = '#a91a1a';
                        itemAuthor.value ? itemAuthor.style.backgroundColor = '#fff' : itemAuthor.style.backgroundColor = '#a91a1a';
                        itemContent.value ? itemContent.style.backgroundColor = '#fff' : itemContent.style.backgroundColor = '#a91a1a';
                    } else {
                        window.location.href = 'rankings.html';
                    }
                });
        });
    });
}