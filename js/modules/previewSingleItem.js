export default function () {
    var url = new URL(window.location.href);
    var itemID = url.searchParams.get('id');
    

    fetch(`http://localhost:3000/posts/${itemID}`)
        .then(function (response) {
            response.json()
                .then(function (posts) {
                    document.querySelector('#rankings-item').innerHTML +=
                `<div class='item'>
                    <div class="item-author"><h2>${posts.title}</h2></div>
                    <div class="item-title"><h3>${posts.author}</h3></div>
                    <div class="item-content"><p>${posts.content}</p></div>
                </div>`;
                });
        });
}