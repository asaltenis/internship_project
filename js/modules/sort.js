export default function (objArray) { 
    var sortState = getSortState();
    var asc = sortState.asc === 'true' ? true : false;
    sortByValue(sortState.column);

    document.querySelectorAll('.click-sort').forEach(sortBtn => {
        sortBtn.addEventListener('click', (e) => {
            const column = e.target.textContent;
            sortByValue(column);
        });
    });

    function sortByValue(sortName) {

        if (sortName.indexOf('ID') > -1) {
            asc ? objArray.sort((a, b) => a.id - b.id) : objArray.sort((a, b) => b.id - a.id);
            reorder(sortName);

        } else if (sortName.indexOf('Title') > -1) {
            objArray.sort((a, b) => {
                var titleA = a.title.toLowerCase();
                var titleB = b.title.toLowerCase();
                if (titleA < titleB) {
                    return asc ? -1 : 1;
                }
                if (titleA > titleB) {
                    return asc ? 1 : -1;
                }
                return 0;
            });
            reorder(sortName);

        } else {
            objArray.sort((a, b) => {
                var authorA = a.author.toLowerCase();
                var authorB = b.author.toLowerCase();
                if (authorA < authorB) {
                    return asc ? -1 : 1;
                }
                if (authorA > authorB) {
                    return asc ? 1 : -1;
                }
                return 0;
            });
            reorder(sortName);
        }

    }

 
    function reorder(sortName) {

        for (var i = 0; i < objArray.length; i++) {
            var row = document.querySelector(`.row-${objArray[i].id}`);
            row.style.order = i;
        }

        setSortSate(sortName, asc);
        asc = !asc;
    }

    function getSortState() {
        var column = localStorage.getItem('sortName') || 'ID';
        var asc = localStorage.getItem('sortDirection') || 'true';
        
        return {
            column,
            asc
        };
    }

    function setSortSate(sortName, isAsc) {
        localStorage.setItem('sortName', sortName);
        localStorage.setItem('sortDirection', isAsc);
    }
}