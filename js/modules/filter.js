export default function () {
    var filterInput = document.querySelector('.filter');

    filterInput.addEventListener('input', (field) => {
        var rows = [...document.querySelectorAll('.item-list-row')];
        var searchValue = field.target.value;

        if (searchValue === '') {
            rows.forEach(row => {
                row.classList.remove('hidden');
            });
            return;
        }

        rows.forEach(row => {
            var rowValue = [...row.querySelectorAll('p')];
            rowValue = rowValue.map(listItem => listItem.textContent);
            rowValue = rowValue.join(' ');
            if (rowValue.includes(searchValue)) {
                row.classList.remove('hidden');
            } else {
                row.classList.add('hidden');
            }
        });
    });
}