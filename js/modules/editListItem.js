export default function () {
    var url = new URL(window.location.href);
    var itemID = url.searchParams.get('id');
    var itemTitle = document.querySelector('#title');
    var itemAuthor = document.querySelector('#author');
    var itemContent = document.querySelector('#content');

    fetch(`http://localhost:3000/posts/${itemID}`)
        .then(function (response) {
            response.json()
                .then(function (posts) {
                    document.querySelector('.form h1').innerHTML += `ITEM ID ${posts.id}`;
                    document.querySelector('#title').value = `${posts.title}`;
                    document.querySelector('#author').value = `${posts.author}`;
                    document.querySelector('#content').value = `${posts.content}`;
                });
        });

    document.querySelector('#edit-button').addEventListener('click', function () {
        fetch(`http://localhost:3000/posts/${itemID}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: itemID,
                title: itemTitle.value,
                author: itemAuthor.value,
                content: itemContent.value
            })
        }).then(function (response) {
            response.json()
                .then(function () {
                    if (!itemTitle.value || !itemAuthor || !itemContent) {
                        itemTitle.value ? itemTitle.style.backgroundColor = '#fff' : itemTitle.style.backgroundColor = '#a91a1a';
                        itemAuthor.value ? itemAuthor.style.backgroundColor = '#fff' : itemAuthor.style.backgroundColor = '#a91a1a';
                        itemContent.value ? itemContent.style.backgroundColor = '#fff' : itemContent.style.backgroundColor = '#a91a1a';
                    } else {
                        window.location.href = 'rankings.html';
                    }
                });
        });
    });
}