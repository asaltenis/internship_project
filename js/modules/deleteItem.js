export default function() {
    document.querySelectorAll('.delete-button').forEach(function (item) {
        item.addEventListener('click', function (event) {
            fetch(`http://localhost:3000/posts/${this.id}`,
                {
                    method: 'DELETE'
                })
                .then(function (response) {
                    response.json()
                        .then(function () {
                            event.target.closest('.grid-list').remove();
                        });
                });
        });
    });
}