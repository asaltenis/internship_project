import sort from './sort.js';
import filter from './filter.js';
import deleteItem from './deleteItem.js';

export default function () {
    var objectArr = [];
    

    fetch('http://localhost:3000/posts').then(function (response) {
        response.json().then(function (posts) {
            for (var item in posts) {
                objectArr.push({

                    id: posts[item].id,
                    author: posts[item].author,
                    title: posts[item].title

                });
                document.querySelector('#rankings-table').innerHTML +=
                    `<div class='grid-list flex-row item-list-row row-${posts[item].id}'>
                        <div class="item-id"><p>${posts[item].id}</p></div>
                        <div class="item-author"><p>${posts[item].author}</p></div>
                        <div class="item-title"><p>${posts[item].title}</p></div>
                        <a href="preview.html?id=${posts[item].id}" class="list-buttons">PREVIEW</a>
                        <a href="edit.html?id=${posts[item].id}" class="list-buttons">EDIT</a>
                        <button id="${posts[item].id}" class="list-buttons delete-button">DELETE</button>
                    </div>`;
            }

            sort(objectArr);
            filter();
            deleteItem();
        });
    });
}