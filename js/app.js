import fullList from './modules/getFullList.js';
import preview from './modules/previewSingleItem.js';
import edit from './modules/editListItem.js';
import add from './modules/addListItem.js';

var url = new URL(window.location.href);
var path = url.pathname;

if (path === '/rankings.html') {
    fullList();
} else if (path === '/preview.html') {
    preview();
} else if (path === '/edit.html') {
    edit();
} else if (path === '/add.html') {
    add();
}