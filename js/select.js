var text = '';
var popup = document.querySelector('#popup');
var popupLinks = document.querySelectorAll('#popup a');
document.addEventListener('mouseup', getSelectedText);

function getSelectedText(event) {
    setTimeout(() => {
        text = document.getSelection().toString();
        if (text !== '') {
            popup.style.display = 'inline-block';
            popup.style.top = `${event.y}px`;
            popup.style.left = `${event.x}px`;
        } else {
            popup.style.display = 'none';
            clearSelection();
        }
    });
}

popupLinks.forEach(function (link) {
    link.addEventListener('click', function () {
        popup.style.display = 'none';
        clearSelection();
    });
});



function clearSelection() {
    if (document.selection) {
        document.selection.empty();
    } else if (window.getSelection) {
        window.getSelection().removeAllRanges();
    }
}