* Project mockup: https://app.moqups.com/arminas.saltenis@gmail.com/JAdaqnvIAb/view/page/aabbb44d0

## QUESTIONS

### TASK 1 questions: 

* **What are most popular code editors among developers?**

   *Atom, 
   Brackets, 
   Sublime, 
   WebStorm, 
   VS Code*

* **What is front-end development?**

   *Front-End development is development of what client sees and interacts with.*

* **What are core technologies every front-end developer must know?**

   *HTML, CSS, Javascript*

* **What are front-end developer titles/types?**

   *Front-End developer, 
   Front-End engineer, 
   Front-End web designer, 
   CSS/HTML developer, 
   Front-End testing/QA*

* **What are responsibilities of front-end developer, web designer and ux designer?**

   *Front-End Developer:
      Implementing HTML, CSS, Javascript and DOM technologies on web platform.*
   
   *Web Designer:
      Has Front-End skills, working with HTML, CSS, but also has professional design skills.*
   
   *UX Designer:
      User Experience designer uses HTML, CSS and, mainly, Javascript, to enhance user satisfaction of using website.*


* **Why are UX mockups useful? Who benefits from them?**

   *UX mockups are useful for Front-End developer, to know where every element should go in design of website.*


* **What is Git? Why is it important?**

   *Git is version control system. It is important beacause it helps to see any changes that were made on projects.*

* **How is version control beneficial?**

   *It is beneficial to be able to get back to previous versions of projects, if any of the changes were unnecessary. Also, it is beneficial for teams, that work on same projects to see all changes without the need to be next to each other.*

* **What is a git repository?**

   *Git repository is where all files and information about changes is stored.*

* **What is code reviewing? What are some best practices for doing code reviews?**

   *It is an eximination of projects source code to find mistakes that were made while developing it.*
   
   *Best practices is:*
   
      to review not more than 400 lines of code at a time;
      to review code less than 60 minutes at a time.



### TASK 2 questions:

* **What is the difference between scripting, programming and markup languages?**

   *Markup languages are languages, that are not, in any way, used to perform any actions. It just structures data.*
   *Scripting languages are languages, that are interpreted when they run, those are languages, that gives computer tasks, that it follows.*
   *Programing languages are languages, that their end result must be compiled.*

* **How the internet works?**

   *It is a technical infrastructure that makes web possible. Basicly, it is a large newtwork, where computer communicate with each other*

* **Research the following: You type in www.delfi.lt into browser and click Enter, whathappens? Describe the full story of what happens and how this request becomes a full HTML page that you can read and interact with?**


* **Which internet browsers are currently most popular among users?**

   *Chrome, 
   Firefox, 
   Internet Explorer, 
   Safari*

* **What is an evergreen browser? What problems does it solve?**

   *Those are browsers, that should update themselves without prompting user.*

* **What is DOCTYPE?**

   *It is an introduction for web browser that lets browser know which HTML version is used.*

* **Do older html files work on newer browsers?**

   *Yes, most old HTML files will work on newer browsers, but some functionality may not work.*

* **What are differences between different DOCTYPES (xHTML, HTML 4.01, HTML5)?**

   *Different DOCTYPES are different specifications of HTML. HTML 4 had loose syntax, it didn't need closing tags. 
   XHTML is strict version of HTML, it doesn't allow mistakes, it is strict about which elemnts can be nested inside each other, also, all tags must be closed.
   HTML5 has new tags, like nav, video, canvas, it is fairly loose syntax, it takes best things from HTML 4.*

* **What are the different HTML5 elements in your page? What is semantic HTML?**

   *header, nav, article, figure, footer. Semantic HTML gives HTML elements meaning, it makes SEO easier.*

* **Do all HTML tags come in pair?**

   *No, there are self closing tags, like input, etc.*

* **Why is SEO important? What ways are the to utilize it?**

   *It makes sites appear on search engine, the better is search engine optimization, the higher in search engine result website appears, more users see and visit site.
   One way to utilize it is to write semantic HTML, adding meta tags with description and using keywords.*

* **What are some of the common list types that can be used when designing a page?**

   *ordered list*
   *unordered list*
   *definition list*
   *menu list*
   *directory list*

* **Should you put quotes around attribute values? Why?**

   *Not in every case, but it's best practise to use quotes on every attribute, because it will make it easier to edit code, and not forget quotes, when you are editing some attribute, that doesn't need quotes, but after changing it might need.* 

* **How do you inserst a text comment in html code?**

   *<!-- -->*

* **What is a hyperlink?**

   *It is a link from a hypertext document to other location or file.*
   


### TASK 3 questions:

* **What does CSS stand for?**

   *Cascading style sheets.*

* **What are style sheets?**

   *A type of file which contains fonts, layout settings, that gives different looks for websites.*

* **What are CSS positioning? What are differences between them?**

   *It specifies a type of position of an element. There are different positions:*
   *static - default position of element. It is not positioned in any special way.*
   *relative - will position element relative to its normal position. *
   *absolute - it is positioned onto its ancestor element.*
   *fixed - it makes element to stick, it doesn't move even when scrolling.*

* **What is CSS box model?**

   *Content, padding, border, margin.*

* **What is the difference between an inline element and a block element?**

   *Block element takes up whole row of elements height, so any other element jumps under it. Inline elements can be next to each other.*

* **What are different ways to center a nested < div > horizontally?**

   *With margins auto, flexbox*

* **What are different ways to center a nested < div > vertically?**

   *With margins auto, flexbox, vertical-align*

* **What is CSS vendor prefixes? Why they are used?**

   *CSS vendor prefixes are strings before CSS property, that will let browsers that do not support that property to work with it.*

* **What is the range for font-weight value?**

   *100 - 900. Increases by 100.*

* **How are active links different from normal links?**

   *Active links are only visible when clicked.*

* **How do you select active link with CSS selector? What are CSS pseudo elements?**

   *:active. CSS pseudo elements gives ability to style elements part, like first letter, change style on hover, etc.*

* **Can several selectors with class names be grouped together?**

   *Yes, by seperating them with ,*

* **What are different ways to use CSS transitions?**

   *With keyframes, property transition.*

* **What are CSS sprites? How can you manipulate them?**

   *It is one image with different images within it, that we can use seperatly. It lets us use one image instead of including few different imgaes.*
   
   
   
### TASK 5 questions:

* **Why Javascript is used on websites? Is it used only on the front-end?**
   
   *To make websites interactive. No, now you can  use Javascript for almost anything, like back-end.*
   
* **What do Javascript functions prompt, alert and confirm do?**

   *Prompt pop-ups dialog window where user can input some text. Alert just pop-ups window with some text for user. Confirm pop-ups window where user can confirm or cancel something.*
   
* **Why is it preferred to include javascript and css code as separate files?**

   *To make it more clean and readable.*
   
* **What datatypes are in javascript?**

   *String, number, array, objects, boolean.*
   
* **What is the difference between == and ===?**

   *== looks for equal values string of '9' will be equal to number 9. === looks for equal value and type, so string of '7' won't be equal to number 7.*
   
* **What is the difference between undefined and null in Javascript?**

   *undefined means that variable is not declared, or its value was not set. null can be set as variables value. undefined is type of data, and null is object.*
   
* **How to get the type of variable?**

   *typeof*
   
* **How to convert number to string? String to number?**

   *Number to string .toString() or '' + 46. String to number Number(), parseInt(), put + or - in front of string, that has numeric value.*
   
* **What does short-circuiting mean in JS context?**

   *Short-circuiting means not reading code to the end. Best example is with || (OR) operator. If first of twho values is true, then js won't even go to second value, because it needs only one true value.*
   
* **What are truthy and falsy value?**

   *These values are not boolean true or false, there values evaluates to true or false if asked. Strings, numbers, arrays, objects are truthy, null, undefined, 0, "" are falsy.*
   
* **How scope works in JS?**

   *Scope is accessibility of variables, functions and objects in code. It provides security, because not every part of variable or any code will be available for the user. There are two different scopes in JS, global and local scopes. Global scope will be available everywhere in code, local scope are variables declared in functions. Lexical scope, where declared variables in functions, that is in function cannot be used by it's parent function, but declared variables in parent function can be used by child function.*
   
* **Explain variable hoisting.**

   *It's declaring variable after it has been used. Declarations of variable is always going to the top of it's scope if it's value hasn't been set. If it's is declared and assigned value, it will be available only within its scope.*
   
* **What is 'use strict'? What are the advantages and disadvantages to using it?**

   *It is like a strict mode of JS. It is used to make functions or JS code more secure. It won't let you use undeclared variables, it will show some silent errors.*
   
* **What is the difference between function declaration and function expression?**

   *Function declaration is named function variable, function expression is can be named or anonymous, but it is part of larger syntax, it, most often, will be set to a variable.*
   
* **What does it mean javascript functions are first-class?**

   *It means, that we can do with them everything, that we can do with other elements, they can be assigned to variable, passed as arguments, included in data structures.*
   
* **How to declare a named function? An anonymous function?**

   *function function_name(arguments); var variable = function(arguments);*
   
* **What is prototype?**

   **
   
* **What is a closure? How to create it?**

   *It is an inner function that has access to outer function's variables. It has three scopes, local, lexixal (parent) and global.*
   
* **What are higher order functions?**

   *It is a function that can receive function as parameter or return a function.*
   
* **What is event bubbling and capturing?**

   *Bubbling is events that happens when we have nested elements with events and they are happening one after another. First on element, that triggered event, then on parent and so on. Capturing is getting to the element that triggered event.*
   
* **What is unit testing? How can it be beneficial?**

   *Is testing of a block of code, not whole code, but just a part of it. It is beneficial, because we find bugs early, debugging becomes easier, because after testing we know that bugs are in latest block of code.*
   
* **Describe Test-driven development.**

   **
   
* **Describe Behaviour-driven development.**

   **
   
* **What is NodeJS?**

   *It is an open source server framework.*
   
* **What are most popular uses of Node?**

   **

* **What is the difference between normal function and arrow function?**

   *=>*